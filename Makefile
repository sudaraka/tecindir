# Makefile: main build script
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
# you are welcome to redistribute it and/or modify it under the terms of the
# BSD 2-clause License. See the LICENSE file for more details.
#

CARGO=cargo

all: build

build: Cargo.* src/*.rs
	$(CARGO) build --release --frozen

install: build
	install -d ${HOME}/bin
	install -m 0755 target/release/tecindir ${HOME}/bin

.PHONY: clean

clean:
	$(CARGO) clean
