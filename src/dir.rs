// dir.rs: directory parsing related subroutines
//
// Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

use crate::{config, db};
use std::ffi::OsStr;
use std::path::PathBuf;

pub struct DirectoryParseContext<'a> {
    pub db: &'a db::Data,
    pub config: &'a config::Config,
    current_depth: u8,
}

impl<'a> DirectoryParseContext<'a> {
    pub fn new(config: &'a config::Config, db: &'a db::Data) -> DirectoryParseContext<'a> {
        DirectoryParseContext {
            db,
            config,
            current_depth: 1,
        }
    }

    fn deeper(&self) -> DirectoryParseContext {
        DirectoryParseContext {
            db: self.db,
            config: self.config,
            current_depth: self.current_depth + 1,
        }
    }
}

pub fn parse(root: &PathBuf, ctx: &DirectoryParseContext) -> Vec<String> {
    let dir_result = root.read_dir();

    match dir_result {
        // If there is an error in reading the directory, gracefully fail
        // further processing the current path.
        Err(_) => vec![],
        Ok(dir) => dir
            .map(|entry| entry.unwrap())
            .flat_map(|file| {
                let filename = file.file_name().into_string().unwrap();
                let fq_filename = root.join(&filename);

                if ctx.config.ignore_dot_files && filename.starts_with(".") {
                    return vec![];
                }

                if let Ok(ft) = file.file_type() {
                    // Get file type data for current entry, and if it is a
                    // directory; perform the look up recursively.
                    if ft.is_dir() {
                        if ctx.config.max_depth <= ctx.current_depth {
                            return vec![];
                        }

                        return parse(&fq_filename, &ctx.deeper());
                    }
                }

                let ext = fq_filename
                    .extension()
                    .and_then(OsStr::to_str)
                    .map(str::to_lowercase)
                    .unwrap_or(String::from(""));
                let ext_type = ctx.db.get_type(&ext);

                match ext_type {
                    None => vec![],
                    Some(et) => vec![et],
                }
            })
            .collect(),
    }
}
