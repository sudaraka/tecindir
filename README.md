# tecindir

Find **tec**hnologies **in** a given **dir**ectory based on file types it
contain, and return appropriate icons representing them.

## License

Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
